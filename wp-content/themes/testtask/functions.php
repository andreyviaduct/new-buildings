<?php

add_action( 'after_setup_theme', 'testtask_theme_setup' );

define( TESTTASK_PRODUCTS_PER_PAGE, 6 );

function testtask_theme_setup() {
	add_theme_support( 'title-tag' );
	add_theme_support( 'woocommerce' );
}

/**
 * Change the breadcrumb separator
 */
add_filter( 'woocommerce_breadcrumb_defaults', 'testtask_breadcrumb_delimiter' );
function testtask_breadcrumb_delimiter( $defaults ) {
	$defaults['delimiter'] = ' &gt; ';

	return $defaults;
}

add_action( 'wp_enqueue_scripts', 'testtask_styles' );

function testtask_styles() {

	wp_enqueue_style( 'icon-font', get_template_directory_uri() . '/fonts/icomoon/icon-font.css' );
	wp_enqueue_style( 'animate', get_template_directory_uri() . '/libs/animate/animate.min.css' );
	wp_enqueue_style( 'theme', get_template_directory_uri() . '/style.css' );

	wp_enqueue_style( 'testtask',
//		get_template_directory_uri() . '/css/style.min.css',
		get_template_directory_uri() . '/css/style.css',
		array()
	);
}

add_action( 'wp_enqueue_scripts', 'testtask_scripts', 10 );

function testtask_scripts() {

	wp_enqueue_script(
		'popper',
		get_template_directory_uri() . '/libs/bootstrap/js/popper.min.js',
		array( 'jquery' ),
		null,
		true
	);

	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/libs/bootstrap/js/bootstrap.min.js', null, null, true );
	wp_enqueue_script( 'ofi', get_template_directory_uri() . '/libs/ofi/ofi.min.js', null, null, true );
	wp_enqueue_script( 'wow', get_template_directory_uri() . '/libs/wowjs/wow.min.js', null, null, true );
	wp_enqueue_script( 'testtask', get_template_directory_uri() . '/js/scripts.js', array( 'jquery' ), '1.0.0', true );

	wp_enqueue_script(
		'custom',
		get_template_directory_uri() . '/js/custom.js',
		array( 'jquery', 'testtask' ),
		filemtime( __DIR__ . '/js/custom.js' ),
		true
	);

}

remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );


add_filter( 'loop_shop_per_page', function ( $number ) {
	return TESTTASK_PRODUCTS_PER_PAGE;
}, 999 );

add_action( 'wp_ajax_testtask_load_more_products', 'testtask_ajax_load_more_products', 10 );
add_action( 'wp_ajax_nopriv_testtask_load_more_products', 'testtask_ajax_load_more_products', 10 );



add_filter( 'posts_orderby', function( $orderby, WP_Query  $wp_query ) {
	global $wpdb;
	if ( 'product' == $wp_query->get( 'post_type' ) ) {
		return "{$wpdb->posts}.menu_order ASC, {$wpdb->posts}.post_title ASC";
	}
	return $orderby;
}, 10, 2);

/**
 * Load more products (with filters)
 */
function testtask_ajax_load_more_products() {
	$current = (int) $_POST['current'];
	$housing = $_POST['housing'];


	$args = array(
		'post_type' => 'product',
		'offset'    => $current * TESTTASK_PRODUCTS_PER_PAGE,
		'posts_per_page' => TESTTASK_PRODUCTS_PER_PAGE,
	);

	if ( $housing ) {
		$args['tax_query'] = array(
			'relation' => 'OR',
			array(
				'taxonomy' => 'pa_building_class',
				'field'    => 'slug',
				'terms'    => $housing,
			),
		);
	}

	$products = new WP_Query( $args );
	$content  = '';

	if ( $products->posts ) {
		while ( $products->have_posts() ) {
			$products->the_post();
			ob_start();
			wc_get_template_part( 'content', 'product' );
			$content .= ob_get_clean();
		}
	} else {
		$content = '';
	}
	wp_send_json_success( [ 'content' => $content, ] );

}

remove_action( 'woocommerce_shop_loop_item_title','woocommerce_template_loop_product_title', 10 );

remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
add_action( 'woocommerce_before_shop_loop_item_title', 'testtask_loop_product_thumbnail', 10 );

function testtask_loop_product_thumbnail() {
	echo '<div class="page-loop__item-image">' . get_the_post_thumbnail( get_the_ID(), 'woocommerce' ). '</div>';
}


require __DIR__ . '/inc/single-product-metaboxes.php';