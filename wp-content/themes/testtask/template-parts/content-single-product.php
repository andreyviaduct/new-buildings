<article class="<?php post_class( 'post', get_the_ID() ); ?>">
    <div class="post-header">
        <h1 class="page-title-h1"><?php the_title(); ?></h1>

        <span>ОАО Брусника</span>

        <div class="post-header__details">

            <div class="address">Новосибирск, Гоголя 14</div>

            <div class="metro"><span class="icon-metro icon-metro--red"></span>Студенческая <span>5 мин.<span
                            class="icon-walk-icon"></span></span></div>

            <div class="metro"><span class="icon-metro icon-metro--green"></span>Сокол <span>25 мин.<span
                            class="icon-bus"></span></span></div>

            <div class="metro"><span class="icon-metro icon-metro--red"></span>Китай-Город <span>15 мин.<span
                            class="icon-bus"></span></span></div>

        </div>


		<?php the_content(); ?>
    </div>
</article>




