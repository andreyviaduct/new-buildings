<?php get_header(); ?>

    <main class="main">

        <div class="container">

            <div class="page-top">
				<?php wp_nav_menu(); ?>

                <!-- <nav class="page-breadcrumb" itemprop="breadcrumb">
                    <a href="/">Главная</a>
                    <span class="breadcrumb-separator"> > </span>
                    <a href="buildings.html">Новостройки</a><span class="breadcrumb-separator"> > </span>
                    Расцветай на Маркса
                </nav>
                -->

            </div>

        </div>

        <div class="page-section">

            <div class="page-content">
				<?php
				if ( have_posts() ):
					while ( have_posts() ):
						the_post();
						?>
                        <article class="post <?php post_class( 'post', get_the_ID() ); ?>">
                            <div class="post-header">
                                <h1 class="page-title-h1"><?php the_title(); ?></h1>


								<?php the_content(); ?>
                            </div>
                        </article>
					<?php

					endwhile;
				endif;
				?>
            </div>

    </main>

<?php get_footer(); ?>