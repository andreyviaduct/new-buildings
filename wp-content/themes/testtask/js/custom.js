/**
 * Javascript for product archive page.
 */
jQuery(function ($) {
    let $params = $("#product-loop-params");
    $("#no-more-items").hide();

    let ajax_get_products = function () {
        $("#button-show-more").hide();
        $("#overlay-load").addClass("overlay-active");
        $("#loading").show();
        let housing_checked = [];
        $("#housing input:checked").each(function () {
            housing_checked.push($(this).val());
        });

        $.ajax(
            wc_add_to_cart_params.ajax_url,
            {
                dataType: 'json',
                method: 'POST',
                data: {
                    action: 'testtask_load_more_products',
                    current: $params.data('current'),
                    housing: housing_checked
                },
                success: function (response) {
                    if (!$params.data('current') || 0 == $params.data('current')) {
                        $("#product-loop").html("");
                    }
                    if (response.data.content) {
                        console.log(response.data.content);
                        $("#product-loop").append(response.data.content);
                        $params.data('current', $params.data('current') + 1);
                        $("#no-more-items").hide();
                        $("#button-show-more").show();
                    } else {
                        $("#no-more-items").show();
                        $("#button-show-more").hide();
                    }
                    $("#loading").hide();
                    $("#overlay-load").removeClass("overlay-active");

                }
            }
        );
    };

    $("#button-show-more").click(function () {
        ajax_get_products();
        return false;
    });
    $("#apply_filter").click(function () {
        $params.data('current', 0);
        ajax_get_products();
        return false;
    });

    $("#reset_filter").click(function () {
        $(".housing input").prop("checked", false);
        setTimeout(function(){$("#apply_filter").click();}, 500);
    });

});
