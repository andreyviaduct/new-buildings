<?php get_header(); ?>

    <main class="main">

        <div class="container">

            <div class="page-top">
				<?php wp_nav_menu(); ?>

                <!-- <nav class="page-breadcrumb" itemprop="breadcrumb">
                    <a href="/">Главная</a>
                    <span class="breadcrumb-separator"> > </span>
                    <a href="buildings.html">Новостройки</a><span class="breadcrumb-separator"> > </span>
                    Расцветай на Маркса
                </nav>
                -->

            </div>

        </div>

        <div class="page-section">

            <div class="page-content">

				<?php woocommerce_content(); ?>

            </div>

    </main>

<?php get_footer(); ?>