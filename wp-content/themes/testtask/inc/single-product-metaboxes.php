<?php

// Register single product metaboxes

add_filter( 'rwmb_meta_boxes', 'testtask_register_single_product_meta_boxes' );

function testtask_register_single_product_meta_boxes($meta_boxes) {
	$prefix = 'product_';
    $meta_boxes[] = [
        'title' => 'Product Fields',
        'post_types' => 'product',
        'include' => [
            'template' => ['woocommerce/single-product.php'],
        ],
        'context'    => 'side',
        'priority'   => 'high',
        'fields'     => [
            [
                'type' => 'text',
                'name' => esc_html__( 'Developer company', 'testtask' ),
                'id'   => $prefix . 'developer_company',
            ],
	        [
		        'name'       => 'Metro',
		        'id'         => $prefix . 'metro_group',
		        'type'       => 'group',
		        'clone'      => true,
		        'sort_clone' => true,
		        'fields'     => [
			        [
				        'name' => 'Station',
				        'id'   => 'station',
				        'type' => 'text',
			        ],
			        [
				        'name' => 'Minutes',
				        'id'   => 'minutes',
				        'type' => 'number',
			        ],
			        [
				        'name' => 'Going by',
				        'id'   => 'going_by',
				        'type' => 'number',
			        ],

		        ],
	        ],
	        [
		        'type'            => 'select',
		        'name'            => esc_html__( 'Constructive', 'testtask' ),
		        'id'              => $prefix . 'constructive',
		        'options'         => [
			        'monolith' => 'Monolith',
		        ],
		        'multiple'        => false,
		        'select_all_none' => true,
	        ],
	        [
		        'type'            => 'select',
		        'name'            => esc_html__( 'Decoration', 'testtask' ),
		        'id'              => $prefix . 'decoration',
		        'options'         => [
			        'full' => 'Full',
		        ],
		        'multiple'        => false,
		        'select_all_none' => true,
	        ],
            [
                'id'            => $prefix . 'map',
                'name'          => esc_html__( 'Location', 'testtask' ),
                'type'          => 'osm',
                'std'           => '55.7501544,37.5321092',
                'address_field' => $prefix . 'address',
            ],
	        [
		        'type' => 'text',
		        'name' => esc_html__( 'Address', 'testtask' ),
		        'id'   => $prefix . 'address',
	        ],
            [
                'type'            => 'select',
                'name'            => esc_html__( 'Basic options', 'testtask' ),
                'id'              => $prefix . 'basic_options',
                'options'         => [
                    'landscaped_courtyard' => 'Landscaped courtyard',
                    'underground_parking' => 'Underground parking',
                    'brick_house' => 'Brick house',
                ],
                'multiple'        => true,
                'select_all_none' => true,
            ],
            [
                'type'            => 'select',
                'name'            => esc_html__( 'Additional options', 'testtask' ),
                'id'              => $prefix . 'additional_options',
                'options'         => [
                    'yard_without_cars' => 'Yard without cars',
                    'high_ceilings' => 'High ceilings',
                    'panoramic_windows' => 'Panoramic windows',
                ],
                'multiple'        => true,
                'select_all_none' => true,
            ],
        ],
    ];
    return $meta_boxes;
}
