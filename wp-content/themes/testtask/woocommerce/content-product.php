<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}


?>

<li <?php wc_product_class( 'page-loop__item wow animate__animated animate__fadeInUp', $product ); ?>
        data-wow-duration="0.8s">
    <a href="#" class="favorites-link favorites-link__add" title="Добавить в Избранное" role="button">
        <span class="icon-heart"><span class="path1"></span><span class="path2"></span></span>
    </a>
	<?php
	/**
	 * Hook: woocommerce_before_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' );
	?>

	<?php
	/**
	 * Hook: woocommerce_before_shop_loop_item_title.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	do_action( 'woocommerce_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	$prefix      = 'product_';
	$post_id     = get_the_ID();
	$address     = get_post_meta( $post_id, $prefix . 'address', true );
	$deadline    = get_post_meta( $post_id, $prefix . 'deadline', true );
	$metro_group = get_post_meta( $post_id, $prefix . 'metro_group', true );
	?>

    <div class="page-loop__item-badges">
		<?php $terms = get_the_terms( $post_id, 'product_tag' );
		foreach ( $terms as $term ) :
			echo '<span class="badge">' . $term->name . '</span>';
		endforeach;
		?>

    </div>


    <div class="page-loop__item-info">
        <h3 class="page-title-h3"><?php the_title(); ?></h3>
        <p class="page-text">Срок сдачи <?php echo $deadline; ?></p>
		<?php
		foreach ( $metro_group as $metro_group_station ) :
			$station = $metro_group_station['station'];
			$minutes = $metro_group_station['minutes'];
			$subway_line_color = $metro_group_station['subway_line_color'];
			$transport = $metro_group_station['transport'];
			?>
            <div class="page-text to-metro">
                <span class="icon-metro <?php echo $subway_line_color; ?>"></span>
                <span class="page-text"><?php echo $station; ?> <span> <?php echo $minutes; ?> мин.</span></span>
                <span class="icon-walk-icon"></span>
            </div>
			<?php
			break;
		endforeach;
		?>
        <span class="page-text text-desc"><?php echo $address; ?></span>
    </div>

	<?php
	do_action( 'woocommerce_after_shop_loop_item' );
	?>
</li>
