<?php

global $product;
$post_id             = get_the_ID();
$prefix              = 'product_';
$developer_company   = get_post_meta( $post_id, $prefix . 'developer_company', true );
$address             = get_post_meta( $post_id, $prefix . 'address', true );
$metro_group         = get_post_meta( $post_id, $prefix . 'metro_group', true );
$constructive        = get_post_meta( $post_id, $prefix . 'constructive', true );
$decoration          = get_post_meta( $post_id, $prefix . 'decoration', true );
$decoration_tool     = get_post_meta( $post_id, $prefix . 'decoration_tool', true );
$deadline            = get_post_meta( $post_id, $prefix . 'deadline', true );
$ceiling_height      = get_post_meta( $post_id, $prefix . 'ceiling_height', true );
$underground_parking = get_post_meta( $post_id, $prefix . 'underground_parking', true );
$number_of_storeys   = get_post_meta( $post_id, $prefix . 'number_of_storeys', true );
$price_group         = get_post_meta( $post_id, $prefix . 'price_group', true );
$rating              = get_post_meta( $post_id, $prefix . 'rating', true );
$map                 = get_post_meta( $post_id, $prefix . 'map', true );


?>
<main class="main">

    <div class="container">

        <div class="page-top">
            <nav class="page-breadcrumb" itemprop="breadcrumb">
                <a href="<?php echo get_home_url(); ?>">Главная</a>
                <span class="breadcrumb-separator"> > </span>
                <a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>">Новостройки</a>
                <span class="breadcrumb-separator"> > </span>
                <?php the_title(); ?>
            </nav>
        </div>

        <div class="page-section">

            <div class="page-content">

                <article class="post <?php post_class( 'post', $post_id ); ?>">

                    <div class="post-header">

                        <h1 class="page-title-h1"><?php the_title(); ?></h1>

                        <span><?php echo $developer_company; ?></span>

                        <div class="post-header__details">

                            <div class="address"><?php echo $address; ?></div>

							<?php foreach ( $metro_group as $metro_group_station ) : ?>
								<?php
								$station           = $metro_group_station['station'];
								$minutes           = $metro_group_station['minutes'];
								$subway_line_color = $metro_group_station['subway_line_color'];
								$transport         = $metro_group_station['transport'];
								?>
                                <div class="metro">
                                    <span class="icon-metro <?php echo $subway_line_color; ?>"></span>
									<?php echo $station; ?> <span><?php echo $minutes; ?> мин.<span
                                                class="<?php echo $transport; ?>"></span></span>
                                </div>
							<?php endforeach; ?>

                        </div>

                    </div>

                    <div class="post-image">

						<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'single-post-thumbnail' ); ?>
                        <img src="<?php echo $image[0]; ?>" data-id="<?php echo $loop->post->ID; ?>"
                             alt="<?php the_title(); ?>">

                        <div class="page-loop__item-badges">
							<?php echo wc_get_product_tag_list( $product->get_id(), ' ' ); ?>
                        </div>

                        <a href="#" class="favorites-link favorites-link__add" title="Добавить в Избранное"
                           role="button">
                            <span class="icon-heart"><span class="path1"></span><span class="path2"></span></span>
                        </a>

                    </div>

                    <h2 class="page-title-h1">Характеристики ЖК</h2>

                    <ul class="post-specs">
                        <li>
                            <span class="icon-building"></span>
                            <div class="post-specs__info">
                                <span>Класс жилья</span>
                                <p>
									<?php
									$building_class = wc_get_product_terms( $post_id, 'pa_building_class' );
									echo $building_class[0]->name;
									?>
                                </p>
                            </div>
                        </li>
                        <li>
                            <span class="icon-brick"></span>
                            <div class="post-specs__info">
                                <span>Конструктив</span>
                                <p><?php echo $constructive; ?></p>
                            </div>
                        </li>
                        <li>
                            <span class="icon-paint"></span>
                            <div class="post-specs__info">
                                <span>Отделка</span>
                                <p><?php echo $decoration; ?>
                                    <span class="tip tip-info" data-toggle="popover" data-placement="top"
                                          data-content="<?php echo $decoration_tool; ?>">
						<span class="icon-prompt"></span>
					</span>
                                </p>
                            </div>
                        </li>
                        <li>
                            <span class="icon-calendar"></span>
                            <div class="post-specs__info">
                                <span>Срок сдачи</span>
                                <p><?php echo $deadline; ?></p>
                            </div>
                        </li>
                        <li>
                            <span class="icon-ruller"></span>
                            <div class="post-specs__info">
                                <span>Высота потолков</span>
                                <p><?php echo $ceiling_height; ?></p>
                            </div>
                        </li>
                        <li>
                            <span class="icon-parking"></span>
                            <div class="post-specs__info">
                                <span>Подземный паркинг</span>
                                <p><?php echo $underground_parking; ?></p>
                            </div>
                        </li>
                        <li>
                            <span class="icon-stair"></span>
                            <div class="post-specs__info">
                                <span>Этажность</span>
                                <p><?php echo $number_of_storeys; ?></p>
                            </div>
                        </li>
                        <li>
                            <span class="icon-wallet"></span>
                            <div class="post-specs__info">
                                <span>Ценовая группа</span>
                                <p><?php echo $price_group; ?></p>
                            </div>
                        </li>
                        <li>
                            <span class="icon-rating"></span>
                            <div class="post-specs__info">
                                <span>Рейтинг</span>
                                <p><?php echo $rating; ?></p>
                            </div>
                        </li>
                    </ul>

                    <h2 class="page-title-h1">Краткое описание</h2>

                    <div class="post-text">
                        <p><?php the_excerpt(); ?></p>
                    </div>

                    <h2 class="page-title-h1">Карта</h2>

                    <div class="post-map">
						<?php
						$args = array(
							'width'       => '100%',
							'height'      => '300px',
							'zoom'        => 14,
							'marker'      => true,
							'marker_icon' => 'http://maps.google.com/mapfiles/ms/micons/blue.png',
						);
						echo rwmb_meta( $prefix . 'map', $args );
						?>

                    </div>

                </article>

            </div>

            <div class="page-filter"></div>

        </div>

    </div>

</main>


